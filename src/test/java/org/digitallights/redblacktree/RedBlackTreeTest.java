package org.digitallights.redblacktree;

import org.digitallights.redblacktree.utils.Color;
import org.digitallights.redblacktree.utils.nodes.Node;
import org.digitallights.redblacktree.utils.rotations.Rotations;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class RedBlackTreeTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    /* Test rotating an unbalanced tree depending on the layout of the elements.
     *
     * Given: 4 unbalanced trees :
     *      1.     1      2. 3        3.    3     4.    1
                  /           \            /             \
     *           2             2          1               3
     *          /               \          \             /
     *         3                 1          2           2
     *
     * Expected: For each tree the result should be:
     *                  2
     *                 / \
     *                1   3
     */
    @Test
    public void testRotations() {
        // RR rotation
        Node<Integer> rootNode = new Node<>(1);

        Node<Integer> node1 = new Node<>(2);
        node1.setParent(rootNode);
        rootNode.setRight(node1);

        Node<Integer> node2 = new Node<>(3);
        node2.setParent(node1);
        node1.setRight(node2);

        rootNode = Rotations.RR_rotation(rootNode);

        assertEquals(2, rootNode.getValue());
        assertEquals(1, rootNode.getLeft().getValue());
        assertEquals(3, rootNode.getRight().getValue());

        //LL rotation
        rootNode = new Node<>(3);

        node1 = new Node<>(2);
        node1.setParent(rootNode);
        rootNode.setLeft(node1);

        node2 = new Node<>(1);
        node2.setParent(node1);
        node1.setLeft(node2);

        rootNode = Rotations.LL_rotation(rootNode);

        assertEquals(2, rootNode.getValue());
        assertEquals(1, rootNode.getLeft().getValue());
        assertEquals(3, rootNode.getRight().getValue());

        // LR rotation
        rootNode = new Node<>(3);

        node1 = new Node<>(1);
        node1.setParent(rootNode);
        rootNode.setLeft(node1);

        node2 = new Node<>(2);
        node2.setParent(node1);
        node1.setRight(node2);

        rootNode = Rotations.LR_rotation(rootNode);

        assertEquals(2, rootNode.getValue());
        assertEquals(1, rootNode.getLeft().getValue());
        assertEquals(3, rootNode.getRight().getValue());

        // RL rotation
        rootNode = new Node<>(1);

        node1 = new Node<>(3);
        node1.setParent(rootNode);
        rootNode.setRight(node1);

        node2 = new Node<>(2);
        node2.setParent(node1);
        node1.setLeft(node2);

        rootNode = Rotations.RL_rotation(rootNode);

        assertEquals(2, rootNode.getValue());
        assertEquals(1, rootNode.getLeft().getValue());
        assertEquals(3, rootNode.getRight().getValue());
    }


    private <T> void assertTreesIdentical(Node<T> expected, Node<T> actual) {
        if (expected == null && actual == null)
            return;

        if (expected == null || actual == null)
            fail();

        if (!expected.equals(actual))
            fail();

        assertTreesIdentical(expected.getRight(), actual.getRight());
        assertTreesIdentical(expected.getLeft(), actual.getLeft());
    }

    /* Test inserting elements in the BST
     *
     * Given: a list of 5 elements:
     *      a = [1, 2, 3, 4, 5]
     *
     * Expected: a balanced BST like this:
     *                  2B
     *                /   \
     *               1B    4B
     *                   /    \
     *                  3R     5R
     *
     *  where B means "black", R means "red"
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testInsertion() throws NoSuchFieldException, IllegalAccessException {
        List<Integer> a = List.of(1, 2, 3, 4, 5);
        RedBlackTree<Integer> integerRedBlackTree = new RedBlackTree<>();

        for (int el : a)
            integerRedBlackTree.insert(el);

        Field root = integerRedBlackTree.getClass().getDeclaredField("root");
        root.setAccessible(true);
        Node<Integer> nodeRoot = (Node<Integer>) root.get(integerRedBlackTree);

        // Expected Red-Black tree
        Node<Integer> expectedRoot = new Node<>(2, Color.BLACK);

        Node<Integer> leftNode1 = new Node<>(1, Color.BLACK);
        leftNode1.setParent(expectedRoot);
        expectedRoot.setLeft(leftNode1);

        Node<Integer> rightNode1 = new Node<>(4, Color.BLACK);
        rightNode1.setParent(expectedRoot);
        expectedRoot.setRight(rightNode1);

        Node<Integer> leftNode2 = new Node<>(3, Color.RED);
        leftNode2.setParent(rightNode1);
        rightNode1.setLeft(leftNode2);

        Node<Integer> rightNode2 = new Node<>(5, Color.RED);
        rightNode2.setParent(rightNode1);
        rightNode1.setRight(rightNode2);

        assertTreesIdentical(expectedRoot, nodeRoot);
    }

    /* Test removal of elements from Red-Black Tree
     *
     * Given: A Red-Black Tree with the following structure
     *
     *                         2B
     *                       /    \
     *               A  =  1B      4B
     *                           /    \
     *                         3R      5R
     *
     *        From "A" the elements are removed in descending order.
     *
     * Expected:
     *
     *             2B                          2B                           2B
     *           /    \                      /    \                       /
     *         1B      4B        ->        1B      3B         ->        1R
     *               /
     *             3R
     *
     *   (5 has been deleted)        (4 has been deleted)        (3 has been deleted)     etc.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testRemoval() throws NoSuchFieldException, IllegalAccessException {
        RedBlackTree<Integer> rbt = new RedBlackTree<>(List.of(1, 2, 3, 4, 5));

        // 5 has been deleted
        rbt.remove(5);

        Field root = rbt.getClass().getDeclaredField("root");
        root.setAccessible(true);
        Node<Integer> nodeRoot = (Node<Integer>) root.get(rbt);

        Node<Integer> expectedRoot = new Node<>(2, Color.BLACK);
        Node<Integer> leftNode1 = new Node<>(1, Color.BLACK);
        leftNode1.setParent(expectedRoot);
        expectedRoot.setLeft(leftNode1);
        Node<Integer> rightNode1 = new Node<>(4, Color.BLACK);
        rightNode1.setParent(expectedRoot);
        expectedRoot.setRight(rightNode1);
        Node<Integer> leftNode2 = new Node<>(3, Color.RED);
        leftNode2.setParent(rightNode1);
        rightNode1.setLeft(leftNode2);

        assertTreesIdentical(expectedRoot, nodeRoot);

        // 4 has been deleted
        rbt.remove(4);

        nodeRoot = (Node<Integer>) root.get(rbt);

        expectedRoot = new Node<>(2, Color.BLACK);
        leftNode1 = new Node<>(1, Color.BLACK);
        leftNode1.setParent(expectedRoot);
        expectedRoot.setLeft(leftNode1);
        rightNode1 = new Node<>(3, Color.BLACK);
        rightNode1.setParent(expectedRoot);
        expectedRoot.setRight(rightNode1);

        assertTreesIdentical(expectedRoot, nodeRoot);

        // 3 has been deleted
        rbt.remove(3);

        nodeRoot = (Node<Integer>) root.get(rbt);

        expectedRoot = new Node<>(2, Color.BLACK);
        leftNode1 = new Node<>(1, Color.RED);
        leftNode1.setParent(expectedRoot);
        expectedRoot.setLeft(leftNode1);

        assertTreesIdentical(expectedRoot, nodeRoot);

        // 2 has been deleted
        rbt.remove(2);

        nodeRoot = (Node<Integer>) root.get(rbt);
        expectedRoot = new Node<>(1, Color.BLACK);

        assertTreesIdentical(expectedRoot, nodeRoot);

        // 1 has been deleted
        rbt.remove(1);

        nodeRoot = (Node<Integer>) root.get(rbt);
        expectedRoot = null;

        assertTreesIdentical(expectedRoot, nodeRoot);
    }
}