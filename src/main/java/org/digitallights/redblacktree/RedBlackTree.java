package org.digitallights.redblacktree;

import org.digitallights.redblacktree.utils.Color;
import org.digitallights.redblacktree.utils.exceptions.ElementDoesNotExistException;
import org.digitallights.redblacktree.utils.nodes.Node;
import org.digitallights.redblacktree.utils.nodes.Nodes;
import org.digitallights.redblacktree.utils.exceptions.ElementAlreadyExistsException;
import org.digitallights.redblacktree.utils.rotations.Rotations;

import java.util.Collection;
import java.util.Comparator;

public class RedBlackTree<T> {
    private Node<T> root;
    private int size;
    private Comparator<T> comparator;

    public RedBlackTree() {
        root = null;
        size = 0;
        comparator = Comparator.comparingInt(Object::hashCode);
    }

    public RedBlackTree(Collection<? extends T> elements) {
        this();
        for (T element : elements)
            insert(element);
    }

//    public void setComparator(Comparator<T> comparator) {
//        this.comparator = comparator;
//    }

    public void insert(T element) throws ElementAlreadyExistsException {
//        if (contains(element))
//            throw new ElementAlreadyExistsException();

        Node<T> newNode = new Node<>(element);
        Node<T> currNode = root, prevNode = null;

        while (currNode != null) {
            prevNode = currNode;
            if (comparator.compare(newNode.getValue(), root.getValue()) < 0)
                currNode = currNode.getLeft();
            else
                currNode = currNode.getRight();
        }

        size++;

        if (prevNode == null) {
            newNode.setColor(Color.BLACK);
            root = newNode;
            return;
        }

        newNode.setColor(Color.RED);
        if (comparator.compare(newNode.getValue(), prevNode.getValue()) < 0) {
            newNode.setParent(prevNode);
            prevNode.setLeft(newNode);
        } else {
            newNode.setParent(prevNode);
            prevNode.setRight(newNode);
        }

        rebalanceInsertion(newNode);
    }

    private void rebalanceInsertion(Node<T> node) {
        Node<T> parent = node.getParent();

        if (node.equals(root)) {
            node.setColor(Color.BLACK);
            return;
        }

        if (node.getParent().equals(root) ||
                !node.getParent().getColor().equals(Color.RED)) return;

        Node<T> grandparent = parent.getParent();
        Node<T> uncle = parent.equals(grandparent.getLeft()) ? grandparent.getRight() : grandparent.getLeft();

        switch (Nodes.checkNodeColor(uncle)) {
            case BLACK -> {
                switch (Rotations.checkRotation(node, parent, grandparent)) {
                    case LL -> {
                        if (grandparent.equals(root))
                            root = Rotations.LL_rotation(grandparent);
                        else
                            Rotations.LL_rotation(grandparent);
                    }
                    case LR -> {
                        if (grandparent.equals(root))
                            root = Rotations.LR_rotation(grandparent);
                        else
                            Rotations.LR_rotation(grandparent);
                    }
                    case RR -> {
                        if (grandparent.equals(root))
                            root = Rotations.RR_rotation(grandparent);
                        else
                            Rotations.RR_rotation(grandparent);
                    }
                    case RL -> {
                        if (grandparent.equals(root))
                            root = Rotations.RL_rotation(grandparent);
                        else
                            Rotations.RL_rotation(grandparent);
                    }
                }
            }
            case RED -> {
                parent.setColor(Color.BLACK);
                uncle.setColor(Color.BLACK);
                grandparent.setColor(Color.RED);
            }
        }

        rebalanceInsertion(grandparent);
    }

    public void remove(T element) throws ElementDoesNotExistException {
//        if (!contains(element))
//            throw new ElementDoesNotExistException();

        Node<T> currNode = root;
        while (currNode.getValue() != element) {
            if (comparator.compare(element, currNode.getValue()) < 0)
                currNode = currNode.getLeft();
            else
                currNode = currNode.getRight();
        }

        switch (Nodes.getChildrenNum(currNode)) {
            case 0 -> {
                Node<T> parent = currNode.getParent();

                if (!currNode.equals(root)) {
                    if (currNode.equals(parent.getLeft()))
                        parent.setLeft(null);
                    else
                        parent.setRight(null);

                    rebalanceRemoval(currNode);
                } else {
                    root = null;
                }

                size--;
            }

            case 1 -> {
                T child;

                if (currNode.getRight() != null)
                    child = currNode.getRight().getValue();
                else
                    child = currNode.getLeft().getValue();

                remove(child);
                currNode.setValue(child);
            }

            case 2 -> {
                T inorderSuccessor = Nodes.getInorderSuccessor(currNode);
                remove(inorderSuccessor);
                currNode.setValue(inorderSuccessor);
            }
        }
    }

    private void rebalanceRemoval(Node<T> node) {
        if (node.equals(root))
            return;

        if (Nodes.checkNodeColor(node).equals(Color.BLACK)) {
            Node<T> sibling = Nodes.getSibling(node);

            switch (sibling.getColor()) {
                case RED -> {
                    Color temp = Nodes.checkNodeColor(node.getParent());
                    node.getParent().setColor(Nodes.checkNodeColor(sibling));
                    sibling.setColor(temp);

                    if (node.getParent().getLeft().equals(sibling)) {
                        Rotations.LL_rotation(node.getParent());
                    } else
                        Rotations.RR_rotation(node.getParent());

                    rebalanceRemoval(node);
                }
                case BLACK -> {
                    if (Nodes.checkNodeColor(Nodes.getSiblingFarChild(node)).equals(Color.BLACK)
                            && Nodes.checkNodeColor(Nodes.getSiblingNearChild(node)).equals(Color.BLACK)) {
                        sibling.setColor(Color.RED);

                        if (Nodes.checkNodeColor(node.getParent()).equals(Color.RED))
                            node.getParent().setColor(Color.BLACK);
                        else
                            rebalanceRemoval(node.getParent());
                    } else if (Nodes.checkNodeColor(Nodes.getSiblingFarChild(node)).equals(Color.BLACK)
                            && Nodes.checkNodeColor(Nodes.getSiblingNearChild(node)).equals(Color.RED)) {
                        Color temp = Nodes.checkNodeColor(sibling);
                        sibling.setColor(Nodes.checkNodeColor(Nodes.getSiblingNearChild(node)));
                        Nodes.getSiblingNearChild(node).setColor(temp);

                        if (node.getParent().getLeft().equals(sibling)) {
                            Rotations.RR_rotation(sibling);
                        } else
                            Rotations.LL_rotation(sibling);

                        rebalanceRemoval(node);
                    } else if (Nodes.checkNodeColor(Nodes.getSiblingFarChild(node)).equals(Color.RED)
                            && Nodes.checkNodeColor(Nodes.getSiblingNearChild(node)).equals(Color.BLACK)) {
                        Color temp = Nodes.checkNodeColor(node.getParent());
                        node.getParent().setColor(Nodes.checkNodeColor(sibling));
                        sibling.setColor(temp);

                        if (node.getParent().getLeft().equals(sibling)) {
                            Rotations.RR_rotation(Nodes.getSibling(node));
                        } else
                            Rotations.LL_rotation(Nodes.getSibling(node));

                        Nodes.getSiblingFarChild(node).setColor(Color.BLACK);
                    }
                }
            }
        }
    }

    public boolean contains(T element) {
        return true;
    }
}
