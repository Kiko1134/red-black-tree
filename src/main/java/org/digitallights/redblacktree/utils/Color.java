package org.digitallights.redblacktree.utils;

public enum Color {
    RED, BLACK
}
