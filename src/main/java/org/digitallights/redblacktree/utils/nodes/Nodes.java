package org.digitallights.redblacktree.utils.nodes;

import org.digitallights.redblacktree.utils.Color;

public class Nodes {
    public static <T> Color checkNodeColor(Node<T> node) {
        if (node == null)
            return Color.BLACK;

        return node.getColor();
    }
    
    public static <T> int getChildrenNum(Node<T> node) {
        if (node.getLeft() != null && node.getRight() != null)
            return 2;
        else if (node.getLeft() != null || node.getRight() != null)
            return 1;
        else
            return 0;
    }

    public static <T> T getInorderSuccessor(Node<T> node) {
        Node<T> root = node.getRight();

        while (root.getLeft() != null) {
            root = root.getLeft();
        }

        return root.getValue();
    }

    public static <T> Node<T> getSibling(Node<T> node) {
        Node<T> parent = node.getParent();
        if (node.equals(parent.getLeft())) {
            return parent.getRight();
        } else return parent.getLeft();
    }

    public static <T> Node<T> getSiblingNearChild(Node<T> node) {
        Node<T> sibling = Nodes.getSibling(node);
        Node<T> parent = node.getParent();
        if (sibling.equals(parent.getRight())) {
            return sibling.getLeft();
        } else return sibling.getRight();
    }

    public static <T> Node<T> getSiblingFarChild(Node<T> node) {
        Node<T> sibling = Nodes.getSibling(node);
        Node<T> parent = node.getParent();
        if (sibling.equals(parent.getRight())) {
            return sibling.getRight();
        } else return sibling.getLeft();
    }
}
