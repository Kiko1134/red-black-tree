package org.digitallights.redblacktree.utils.nodes;

import org.digitallights.redblacktree.utils.Color;

import java.util.Objects;

public class Node<T> {
    private T value;
    private Node<T> parent;
    private Node<T> left;
    private Node<T> right;
    private Color color;

    public Node(T value) {
        this.value = value;
    }

    public Node(T value, Color color) {
        this.value = value;
        this.color = color;
        left = null;
        right = null;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Node<T> getLeft() {
        return left;
    }

    public void setLeft(Node<T> left) {
        this.left = left;
    }

    public Node<T> getRight() {
        return right;
    }

    public void setRight(Node<T> right) {
        this.right = right;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Node<T> getParent() {
        return parent;
    }

    public void setParent(Node<T> parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node<?> node = (Node<?>) o;
        return value.equals(node.value) && color == node.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, color);
    }
}
