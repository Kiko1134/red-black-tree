package org.digitallights.redblacktree.utils.rotations;

public enum RotationType {
    LL, RR, LR, RL
}
