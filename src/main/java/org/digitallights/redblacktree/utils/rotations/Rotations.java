package org.digitallights.redblacktree.utils.rotations;

import org.digitallights.redblacktree.utils.Color;
import org.digitallights.redblacktree.utils.nodes.Node;

public class Rotations {
    public static <T> Node<T> RR_rotation(Node<T> node) {
        Node<T> p = node.getParent();
        Node<T> r1 = node.getRight();

        node.setRight(r1.getLeft());

        if (r1.getLeft() != null)
            r1.getLeft().setParent(node);

        r1.setLeft(node);
        node.setParent(r1);
        r1.setParent(p);

        if (p != null) {
            if (p.getRight().equals(node))
                p.setRight(r1);
            else
                p.setLeft(r1);
        }

        Color c = node.getColor();
        node.setColor(r1.getColor());
        r1.setColor(c);

        return r1;
    }

    public static <T> Node<T> LL_rotation(Node<T> node) {
        Node<T> p = node.getParent();
        Node<T> l1 = node.getLeft();

        node.setLeft(l1.getRight());

        if (l1.getRight() != null)
            l1.getRight().setParent(node);

        l1.setRight(node);
        node.setParent(l1);
        l1.setParent(p);

        if (p != null) {
            if (p.getRight().equals(node))
                p.setRight(l1);
            else
                p.setLeft(l1);
        }

        Color c = node.getColor();
        node.setColor(l1.getColor());
        l1.setColor(c);

        return l1;
    }

    public static <T> Node<T> RL_rotation(Node<T> node) {
        Node<T> r = node.getRight();
        Node<T> l = r.getLeft();

        node.setRight(l);
        l.setParent(node);

        r.setLeft(l.getRight());

        r.setParent(l);
        l.setRight(r);

        return RR_rotation(node);
    }

    public static <T> Node<T> LR_rotation(Node<T> node) {
        Node<T> l = node.getLeft();
        Node<T> r = l.getRight();

        node.setLeft(r);
        r.setParent(node);

        l.setRight(r.getLeft());

        l.setParent(r);
        r.setLeft(l);

        return LL_rotation(node);
    }

    public static <T> RotationType checkRotation(Node<T> node, Node<T> parent, Node<T> grandparent) {
        boolean[] RR = new boolean[2];

        RR[0] = grandparent.getRight().equals(parent);
        RR[1] = parent.getRight().equals(node);

        if (RR[0] && RR[1])
            return RotationType.RR;

        if (RR[0] && !RR[1])
            return RotationType.RL;

        if (!RR[0] && RR[1])
            return RotationType.LR;

        return RotationType.LL;
    }
}
